var ufloFltr=angular.module('myfltr',[]);
	
	ufloFltr.filter('ucwords', function(){
        return function(input){
            var res='';
            var res_cal=input.split(' ');
            for (var i=0;i<res_cal.length;i++) {
                var Rijec = res_cal[i].substring(0, 1).toUpperCase()+res_cal[i].substring(1).toLowerCase();
                var res = res + ' ' + Rijec;
            }
            return res;
        }
    });
    
    //filter ucfirst - Samo prva rijec u recenici ima prvo slovo veliko
    ufloFltr.filter('ucfirst', function(){
        return function(input){
            return input.substring(0,1).toUpperCase() + input.substring(1).toLowerCase();
        }
    });

   
    ufloFltr.directive('isActiveNav', [ '$location', function($location) {

    return {
         restrict: 'A',
         link: function(scope, element) {
           scope.location = $location;
           scope.$watch('location.path()', function(currentPath) {

             if('#' + currentPath === element[0].attributes['href'].nodeValue) {
               element.parent().addClass('active');
             } else {
               element.parent().removeClass('active');
             }
           });
        }
     };

    }]);