var uflo=angular.module('uflo',['ngRoute','ufloCtrl','myfltr']);

uflo.config(['$routeProvider',function($routeProvider){

	$routeProvider
		.when('/',{
			templateUrl:'templates/pro.html',
			controller :'ufloMyCtrl'

		})
		.when('/ps',{
			templateUrl:'templates/pro.html',
			controller :'ufloMyCtrl'

		})
		.when('/bio',{
			templateUrl:'templates/biog.html',
			controller :'ufloMyCtrl'			
		})
		/*.when('/contact',{
			templateUrl:'templates/bio.html',
			controller :'ufloMyCtrl'
		})*/
		.otherwise({
			templateUrl:'templates/paro.html'	
		})

}])
